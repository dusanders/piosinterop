﻿namespace PiOSInterop.Interfaces.NativeProcess
{
    /// <summary>
    /// Interface 
    /// </summary>
    public interface INativeConfiguration
    {
        /// <summary>
        /// Native device interface name for connecting to wifi networks
        /// </summary>
        string WifiInterface { get; }

        /// <summary>
        /// Native device interface name for hosting the AP
        /// </summary>
        string APInterface { get; }

        /// <summary>
        /// Millisecond wait time between the scan command and checking scan results
        /// </summary>
        int WifiScanTime { get; }

        /// <summary>
        /// Millisecond wait time between checks for wifi connection state queries
        /// </summary>
        int WifiStateWaitTime { get; }
    }
}
