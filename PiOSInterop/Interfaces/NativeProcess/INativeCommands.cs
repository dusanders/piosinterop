﻿namespace PiOSInterop.Interfaces.NativeProcess
{
    /// <summary>
    /// Interface defining native OS wifi commands and magic strings
    /// </summary>
    public interface INativeCommands
    {
        /// <summary>
        /// Key value for determining the connection state from the native output
        /// </summary>
        string STATE_ID { get; }

        /// <summary>
        /// Key value for determining the security flags from the native output
        /// </summary>
        string SECURITY_ID { get; }

        /// <summary>
        /// Key value for determining the IP from the native output
        /// </summary>
        string IP_ID { get; }

        /// <summary>
        /// Key value for determining the SSID from the native output
        /// </summary>
        string SSID_ID { get; }

        /// <summary>
        /// Key value for determining the network ID from the native output
        /// </summary>
        string ID_ID { get; }

        /// <summary>
        /// Key value for determining the frequency from the native output
        /// </summary>
        string FREQ_ID { get; }

        /// <summary>
        /// Key value for determining the AP's MAC address from the native output
        /// </summary>
        string MAC_ID { get; }

        /// <summary>
        /// Value representing the delimiter used within the native system to separate the
        /// key/value pairs within the network information output
        /// </summary>
        string NETWORK_INFO_DELIMITER { get; }

        /// <summary>
        /// Absolute path to use when calling the native wifi controller application. EG. wpa_cli
        /// </summary>
        string WPA_CLI_BINARY { get; }

        /// <summary>
        /// Value representing a connected network state
        /// </summary>
        string WPA_CONNECTED { get; }

        /// <summary>
        /// Value representing an inactive network state
        /// </summary>
        string WPA_INACTIVE { get; }

        /// <summary>
        /// Value representing a scanning network state
        /// </summary>
        string WPA_SCANNING { get; }

        /// <summary>
        /// Value representing the delimiter used within the native system to separate the 
        /// values when listing networks
        /// </summary>
        string WPA_OUTPUT_DELIMITER { get; }

        /// <summary>
        /// Value used to specify the network id during wifi commands
        /// </summary>
        string WPA_SET_NETWORK { get; }

        /// <summary>
        /// Value used to specify the enable command
        /// </summary>
        string WPA_ENABLE_NETWORK { get; }

        /// <summary>
        /// Value used to specify the add network command
        /// </summary>
        string WPA_ADD_NETWORK { get; }

        /// <summary>
        /// Value used to specify removing a network command
        /// </summary>
        string WPA_REMOVE_NETWORK { get; }

        /// <summary>
        /// Value used to specify the reconfigure command
        /// </summary>
        string WPA_RECONFIGURE { get; }

        /// <summary>
        /// Value used to specify the list networks command
        /// </summary>
        string WPA_LIST_NETWORKS { get; }

        /// <summary>
        /// Value used to specify the associate command
        /// </summary>
        string WPA_ASSOCIATE { get; }

        /// <summary>
        /// Value used to specify the disassociate command
        /// </summary>
        string WPA_DISASSOCIATE { get; }

        /// <summary>
        /// Value used to specify the status command
        /// </summary>
        string WPA_STATUS { get; }

        /// <summary>
        /// Value used to specify the scan command
        /// </summary>
        string WPA_SCAN { get; }

        /// <summary>
        /// Value used to specify the scan results command
        /// </summary>
        string WPA_SCAN_RESULTS { get; }

        /// <summary>
        /// Value used to specify the save command
        /// </summary>
        string WPA_SAVE { get; }

        /// <summary>
        /// Value used to specify the SSID value when configuring a network
        /// </summary>
        string WPA_SSID { get; }

        /// <summary>
        /// Value used to specify the PSK value when configuring a network
        /// </summary>
        string WPA_PSK { get; }

        /// <summary>
        /// Value representing a failed native output
        /// </summary>
        string WPA_FAILED { get; }

        /// <summary>
        /// Value representing a success native output
        /// </summary>
        string WPA_SUCCESS { get; }
    }
}
