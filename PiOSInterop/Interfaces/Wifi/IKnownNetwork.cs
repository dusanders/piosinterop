﻿namespace PiOSInterop.Interfaces.Wifi
{
    public interface IKnownNetwork
    {
        /// <summary>
        /// Index of the known network within the known networks list
        /// </summary>
        string Index { get; }

        /// <summary>
        /// SSID value of the known network
        /// </summary>
        string SSID { get; }
    }
}
