﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace PiOSInterop.Interfaces.Wifi
{
    /// <summary>
    /// Interface defining members and methods for controlling the native wifi interface
    /// </summary>
    public interface INativeWifiService
    {
        /// <summary>
        /// Property to flag if currently connected to a wifi network
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// Property to flag if wifi interface is currently active
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// <see cref="IAvailableNetwork"/> representing the currently connected network
        /// </summary>
        IAvailableNetwork CurrentNetwork { get; }

        /// <summary>
        /// <see cref="ObservableCollection{T}"/> of <see cref="IAvailableNetwork"/> objects representing available wifi networks
        /// </summary>
        ObservableCollection<IAvailableNetwork> AvailableNetworks { get; }

        /// <summary>
        /// Event fired when <see cref="CurrentNetwork"/> property changes
        /// </summary>
        event EventHandler<IAvailableNetwork> CurrentNetworkChanged;

        /// <summary>
        /// Event fired when <see cref="IsConnected"/> property changes state
        /// </summary>
        event EventHandler<bool> ConnectedStateChanged;

        /// <summary>
        /// Event fired when <see cref="IsActive"/> property changes state
        /// </summary>
        event EventHandler<bool> IsActiveStateChanged;

        /// <summary>
        /// Method to scan for available wifi networks
        /// </summary>
        /// <returns>True if scanning started, False if error</returns>
        Task<bool> ScanWifiAsync();

        /// <summary>
        /// Method to stop scanning for available wifi networks
        /// </summary>
        /// <returns>True if stopped scanning, false if error</returns>
        Task<bool> StopScanWifiAsync();

        /// <summary>
        /// Method to add a new network to the list of known networks
        /// </summary>
        /// <param name="ssid">SSID of the network</param>
        /// <param name="passkey">Passphrase of the network - null if none</param>
        /// <returns>True if added, false if error</returns>
        Task<bool> AddNetworkAsync(string ssid, string passphrase = null);

        /// <summary>
        /// Method to connect to a wifi network
        /// </summary>
        /// <param name="ssid">SSID of the network to connect to</param>
        /// <returns>True if connected, false if failed.</returns>
        Task<bool> ConnectAsync(string ssid);

        /// <summary>
        /// Method to disconnect from current wifi network
        /// </summary>
        /// <returns>True if disconnected, false if failed.</returns>
        Task<bool> DisconnectAsync();

        /// <summary>
        /// Method to remove a network from the list of known networks
        /// </summary>
        /// <param name="ssid">SSID of network to remove</param>
        /// <returns>True if removed, false if error</returns>
        Task<bool> RemoveNetworkAsync(string ssid);

        /// <summary>
        /// Method to get the list of known networks
        /// </summary>
        /// <returns><see cref="List{T}"/> of <see cref="IKnownNetwork"/> objects</returns>
        Task<List<IKnownNetwork>> GetKnownNetworksAsync();
    }
}