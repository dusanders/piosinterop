﻿namespace PiOSInterop.Interfaces.Wifi
{
    /// <summary>
    /// Interface defining members to represent an available wifi network
    /// </summary>
    public interface IAvailableNetwork
    {
        /// <summary>
        /// Network SSID
        /// </summary>
        string SSID { get; }

        /// <summary>
        /// AP MAC Address
        /// </summary>
        string MAC { get; }

        /// <summary>
        /// Wifi network frequency
        /// </summary>
        string Freq { get; }

        /// <summary>
        /// Wifi network signal level
        /// </summary>
        string SignalLevel { get; }

        /// <summary>
        /// Wifi network flags
        /// </summary>
        string Flags { get; }
    }
}
