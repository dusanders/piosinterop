﻿using PiOSInterop.Interfaces.NativeProcess;

namespace PiOSInterop.Implementations.NativeProcess
{
    /// <summary>
    /// Class to implement <see cref="INativeCommands"/> interface
    /// </summary>
    public class NativeCommands : INativeCommands
    {
        /// <summary>
        /// Value representing a connected network state
        /// </summary>
        public string WPA_CONNECTED { get; set; }

        /// <summary>
        /// Value representing an inactive network state
        /// </summary>
        public string WPA_INACTIVE { get; set; }

        /// <summary>
        /// Value representing a scanning network state
        /// </summary>
        public string WPA_SCANNING { get; set; }

        /// <summary>
        /// Absolute path to use when calling the native wifi controller application. EG. wpa_cli
        /// </summary>
        public string WPA_CLI_BINARY { get; set; }

        /// <summary>
        /// Value representing the delimiter used within the native system to separate the 
        /// values when listing networks
        /// </summary>
        public string WPA_OUTPUT_DELIMITER { get; set; }

        /// <summary>
        /// Value representing the delimiter used within the native system to separate the
        /// key/value pairs within the network information output
        /// </summary>
        public string NETWORK_INFO_DELIMITER { get; set; }

        /// <summary>
        /// Key value for determining the connection state from the native output
        /// </summary>
        public string STATE_ID { get; set; }

        /// <summary>
        /// Key value for determining the AP's MAC address from the native output
        /// </summary>
        public string MAC_ID { get; set; }

        /// <summary>
        /// Key value for determining the frequency from the native output
        /// </summary>
        public string FREQ_ID { get; set; }

        /// <summary>
        /// Key value for determining the network ID from the native output
        /// </summary>
        public string ID_ID { get; set; }

        /// <summary>
        /// Key value for determining the SSID from the native output
        /// </summary>
        public string SSID_ID { get; set; }

        /// <summary>
        /// Key value for determining the IP from the native output
        /// </summary>
        public string IP_ID { get; set; }

        /// <summary>
        /// Key value for determining the security flags from the native output
        /// </summary>
        public string SECURITY_ID { get; set; }

        /// <summary>
        /// Value used to specify the add network command
        /// </summary>
        public string WPA_ADD_NETWORK { get; set; }

        /// <summary>
        /// Value used to specify removing a network command
        /// </summary>
        public string WPA_REMOVE_NETWORK { get; set; }

        /// <summary>
        /// Value used to specify the reconfigure command
        /// </summary>
        public string WPA_RECONFIGURE { get; set; }

        /// <summary>
        /// Value used to specify the associate command
        /// </summary>
        public string WPA_ASSOCIATE { get; set; }

        /// <summary>
        /// Value used to specify the disassociate command
        /// </summary>
        public string WPA_DISASSOCIATE { get; set; }

        /// <summary>
        /// Value used to specify the scan command
        /// </summary>
        public string WPA_SCAN { get; set; }

        /// <summary>
        /// Value used to specify the scan results command
        /// </summary>
        public string WPA_SCAN_RESULTS { get; set; }

        /// <summary>
        /// Value used to specify the network id during wifi commands
        /// </summary>
        public string WPA_SET_NETWORK { get; set; }

        /// <summary>
        /// Value used to specify the save command
        /// </summary>
        public string WPA_SAVE { get; set; }

        /// <summary>
        /// Value used to specify the SSID value when configuring a network
        /// </summary>
        public string WPA_SSID { get; set; }

        /// <summary>
        /// Value used to specify the PSK value when configuring a network
        /// </summary>
        public string WPA_PSK { get; set; }

        /// <summary>
        /// Value representing a failed native output
        /// </summary>
        public string WPA_FAILED { get; set; }

        /// <summary>
        /// Value representing a success native output
        /// </summary>
        public string WPA_SUCCESS { get; set; }

        /// <summary>
        /// Value used to specify the enable command
        /// </summary>
        public string WPA_ENABLE_NETWORK { get; set; }

        /// <summary>
        /// Value used to specify the list networks command
        /// </summary>
        public string WPA_LIST_NETWORKS { get; set; }

        /// <summary>
        /// Value used to specify the status command
        /// </summary>
        public string WPA_STATUS { get; set; }
    }
}
