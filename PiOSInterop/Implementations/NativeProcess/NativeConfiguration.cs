﻿using PiOSInterop.Interfaces.NativeProcess;

namespace PiOSInterop.Implementations.NativeProcess
{
    /// <summary>
    /// Class to implement <see cref="INativeConfiguration"/> interface
    /// </summary>
    public class NativeConfiguration : INativeConfiguration
    {
        #region Public Members

        /// <summary>
        /// Native device interface name for connecting to wifi networks
        /// </summary>
        public string WifiInterface { get; set; }

        /// <summary>
        /// Native device interface name for hosting the AP
        /// </summary>
        public string APInterface { get; set; }

        /// <summary>
        /// Millisecond wait time between the scan command and checking scan results
        /// </summary>
        public int WifiScanTime { get; set; }

        /// <summary>
        /// Millisecond wait time between checks for wifi connection state queries
        /// </summary>
        public int WifiStateWaitTime { get; set; }

        #endregion
    }
}
