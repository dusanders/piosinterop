﻿using PiOSInterop.Interfaces.Wifi;

namespace PiOSInterop.Implementations.Wifi
{
    /// <summary>
    /// Class implementing <see cref="IKnownNetwork"/> interface
    /// </summary>
    public class KnownNetwork : IKnownNetwork
    {
        #region Private Members

        /// <summary>
        /// Backing store for <see cref="Index"/>
        /// </summary>
        private string _index;

        /// <summary>
        /// Backing store for <see cref="SSID"/>
        /// </summary>
        private string _ssid;

        #endregion

        #region Public Members

        /// <summary>
        /// Current index within known network list
        /// </summary>
        public string Index
        {
            get { return _index; }
        }

        /// <summary>
        /// SSID value of network
        /// </summary>
        public string SSID
        {
            get { return _ssid; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="index">Index within known network list</param>
        /// <param name="ssid">SSID value of the network</param>
        public KnownNetwork(string index, string ssid)
        {
            _index = index;
            _ssid = ssid;
        }

        #endregion
    }
}
