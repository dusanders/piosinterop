﻿using PiOSInterop.Interfaces.Wifi;

namespace PiOSInterop.Implementations.Wifi
{
    /// <summary>
    /// Class to implement <see cref="IAvailableNetwork"/> interface
    /// </summary>
    public class AvailableNetwork : IAvailableNetwork
    {
        #region Private Members

        /// <summary>
        /// Backing store for <see cref="SSID"/>
        /// </summary>
        private string _ssid;

        /// <summary>
        /// Backing store for <see cref="MAC"/>
        /// </summary>
        private string _mac;

        /// <summary>
        /// Backing store for <see cref="Freq"/>
        /// </summary>
        private string _freq;

        /// <summary>
        /// Backing store for <see cref="SignalLevel"/>
        /// </summary>
        private string _signalLevel;

        /// <summary>
        /// Backing store for <see cref="Flags"/>
        /// </summary>
        private string _flags;

        #endregion

        #region Public Members

        /// <summary>
        /// Network SSID
        /// </summary>
        public string SSID
        {
            get { return _ssid; }
        }

        /// <summary>
        /// AP MAC Address
        /// </summary>
        public string MAC
        {
            get { return _mac; }
        }

        /// <summary>
        /// Network broadcast frequency
        /// </summary>
        public string Freq
        {
            get { return _freq; }
        }

        /// <summary>
        /// Network signal level
        /// </summary>
        public string SignalLevel
        {
            get { return _signalLevel; }
        }

        /// <summary>
        /// Network flags
        /// </summary>
        public string Flags
        {
            get { return _flags; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="ssid">Network SSID</param>
        /// <param name="mac">Network MAC</param>
        /// <param name="freq">Network Frequency</param>
        /// <param name="signalLevel">Network signal level</param>
        /// <param name="flags">Network flags</param>
        public AvailableNetwork(string ssid,
                                string mac,
                                string freq,
                                string signalLevel,
                                string flags)
        {
            _ssid = ssid;
            _mac = mac;
            _freq = freq;
            _signalLevel = signalLevel;
            _flags = flags;
        }

        #endregion
    }
}
