﻿using PiOSInterop.Interfaces.NativeProcess;
using PiOSInterop.Interfaces.Wifi;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace PiOSInterop.Implementations.Wifi
{
    /// <summary>
    /// Class implementing <see cref="INativeWifiService"/> interface
    /// </summary>
    public class NativeWifiService : INativeWifiService, IDisposable
    {
        #region Dependencies

        /// <summary>
        /// Instance of <see cref="INativeCommands"/> for launching native applications
        /// </summary>
        private INativeCommands _nativeCommands;

        /// <summary>
        /// Instance of <see cref="INativeConfiguration"/> for getting OS specific values (eg, wifi interfaces)
        /// </summary>
        private INativeConfiguration _nativeConfiguration;

        #endregion

        #region Private Members

        #region Constants

        /// <summary>
        /// Enum for debug output levels
        /// </summary>
        private enum DEBUG_LEVEL
        {
            INFO = 0,
            WARN = 1,
            ERROR = 2
        }

        #endregion

        /// <summary>
        /// Backing store for <see cref="IsConnected"/> member
        /// </summary>
        private bool _isConnected;

        /// <summary>
        /// Backing store for <see cref="IsActive"/> member
        /// </summary>
        private bool _isActive;

        /// <summary>
        /// Backing store for <see cref="CurrentNetwork"/>
        /// </summary>
        private IAvailableNetwork _currentNetwork;

        /// <summary>
        /// <see cref="List{T}"/> of <see cref="IKnownNetwork"/> objects representing the current list of known networks
        /// </summary>
        private List<IKnownNetwork> _knownNetworks;

        /// <summary>
        /// Flag to indicate we are currently parsing the known networks from the native system
        /// </summary>
        private bool _isUpdatingKnownNetworks = false;

        /// <summary>
        /// <see cref="CancellationTokenSource"/> to provice <see cref="CancellationToken"/> for the scanning thead
        /// </summary>
        private CancellationTokenSource _scanTokenSource = null;

        /// <summary>
        /// <see cref="CancellationTokenSource"/> to provide <see cref="CancellationToken"/> for monitoring connection state thread
        /// </summary>
        private CancellationTokenSource _connectedMonitor = null;
        
        #endregion

        #region Public Members

        /// <summary>
        /// <see cref="ObservableCollection{T}"/> of <see cref="IAvailableNetwork"/> objects representing available wifi networks
        /// </summary>
        public ObservableCollection<IAvailableNetwork> AvailableNetworks { get; private set; }

        /// <summary>
        /// Event fired when <see cref="CurrentNetwork"/> property changes
        /// </summary>
        public event EventHandler<IAvailableNetwork> CurrentNetworkChanged;

        /// <summary>
        /// Event fired when <see cref="IsConnected"/> property changes state
        /// </summary>
        public event EventHandler<bool> ConnectedStateChanged;

        /// <summary>
        /// Event fired when <see cref="IsActive"/> property changes state
        /// </summary>
        public event EventHandler<bool> IsActiveStateChanged;

        /// <summary>
        /// Property to flag if currently connected to a wifi network
        /// </summary>
        public bool IsConnected
        {
            get { return _isConnected; }
            private set
            {
                if (value == _isConnected)
                    return;
                _isConnected = value;
                ConnectedStateChanged?.Invoke(this, _isConnected);
            }
        }   

        /// <summary>
        /// Property to flag if wifi interface is currently active
        /// </summary>
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                if (value == _isActive)
                    return;
                _isActive = value;
                IsActiveStateChanged?.Invoke(this, _isActive);
            }
        }

        /// <summary>
        /// <see cref="IAvailableNetwork"/> representing the currently connected network
        /// </summary>
        public IAvailableNetwork CurrentNetwork
        {
            get { return _currentNetwork; }
            private set
            {
                if (value != null && value.MAC.Equals(_currentNetwork.MAC))
                {
                    return;
                }
                _currentNetwork = value;
                CurrentNetworkChanged.Invoke(this, _currentNetwork);
            }
        }

        #endregion

        #region Constructor

        public NativeWifiService(INativeCommands nativeCommands,
                            INativeConfiguration nativeConfiguration)
        {
            _nativeCommands = nativeCommands;
            _nativeConfiguration = nativeConfiguration;
            LazyUpdateKnownNetworks();
            MonitorConnectionState();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Method to get the list of known networks
        /// </summary>
        /// <returns><see cref="List{T}"/> of <see cref="IKnownNetwork"/> objects</returns>
        public async Task<List<IKnownNetwork>> GetKnownNetworksAsync()
        {
            var listNetworksArg = $"{_nativeCommands.WPA_LIST_NETWORKS}";
            var response = await RunWPAProcessForMultiLineResult(listNetworksArg, 2);
            var result = new List<IKnownNetwork>();
            for(int i=0; i<response.Count; i++)
            {
                var splitLine = response[i].Split(_nativeCommands.WPA_OUTPUT_DELIMITER);
                var knownNetwork = new KnownNetwork(splitLine[0], splitLine[1]);
                result.Add(knownNetwork);
            }
            return result;
        }

        /// <summary>
        /// Method to add a new network to the list of known networks
        /// </summary>
        /// <param name="ssid">SSID of the network</param>
        /// <param name="passkey">Passphrase of the network - null if none</param>
        /// <returns>True if added, false if error</returns>
        public async Task<bool> AddNetworkAsync(string ssid, string passkey = null)
        {
            var addNetworkArgument = $"{_nativeCommands.WPA_ADD_NETWORK}";
            var newIndex = await RunWPAProcessForStringResult(addNetworkArgument);
            if (newIndex.Contains(_nativeCommands.WPA_FAILED))
                return false;
            var assignSSIDArg = $"{_nativeCommands.WPA_SET_NETWORK} {newIndex} {_nativeCommands.WPA_SSID} \'\"{ssid}\"\'";
            var assignPSKArg = $"{_nativeCommands.WPA_SET_NETWORK} {newIndex} {_nativeCommands.WPA_PSK} \'\"{passkey}\"\'";
            if (!await RunWPAProcessForBoolResult(assignSSIDArg))
            {
                DebugOutput($"Failed to assign SSID: {ssid}", level: DEBUG_LEVEL.ERROR);
                return false;
            }
            if (!string.IsNullOrWhiteSpace(passkey))
            {
                if (!await RunWPAProcessForBoolResult(assignPSKArg))
                {
                    DebugOutput($"Failed to set psk: {passkey} for network: {ssid}", level: DEBUG_LEVEL.ERROR);
                    return false;
                }
            }
            var saveArg = $"{_nativeCommands.WPA_SAVE}";
            if (!await RunWPAProcessForBoolResult(saveArg))
            {
                DebugOutput($"Failed to save configuration on network: {ssid}", level: DEBUG_LEVEL.ERROR);
                return false;
            }
            LazyUpdateKnownNetworks();
            return true;
        }

        /// <summary>
        /// Method to remove a network from the list of known networks
        /// </summary>
        /// <param name="ssid">SSID of network to remove</param>
        /// <returns>True if removed, false if error</returns>
        public async Task<bool> RemoveNetworkAsync(string ssid)
        {
            while (_isUpdatingKnownNetworks)
                await Task.Delay(30);
            if(_knownNetworks== null || _knownNetworks.Count < 1)
            {
                DebugOutput($"Known networks list is empty!", level: DEBUG_LEVEL.WARN);
                return true;
            }
            var foundNetwork = _knownNetworks.FirstOrDefault(network => network.SSID.Equals(ssid));
            if (foundNetwork == null)
            {
                DebugOutput($"Network: {ssid} was not found in known networks.", level: DEBUG_LEVEL.WARN);
                return true;
            }
            var removeNetworkArg = $"{_nativeCommands.WPA_REMOVE_NETWORK} {foundNetwork.Index}";
            if (! await RunWPAProcessForBoolResult(removeNetworkArg))
            {
                DebugOutput($"Failed to remove network: {ssid}", level: DEBUG_LEVEL.ERROR);
                return false;
            }
            var saveArg = $"{_nativeCommands.WPA_SAVE}";
            if(!await RunWPAProcessForBoolResult(saveArg))
            {
                DebugOutput($"Failed to save config when removing network: {ssid}", level: DEBUG_LEVEL.ERROR);
                return false;
            }
            LazyUpdateKnownNetworks();
            return true;
        }

        /// <summary>
        /// Method to connect to a wifi network
        /// </summary>
        /// <param name="ssid">SSID of the network to connect to</param>
        /// <returns>True if connected, false if failed.</returns>
        public Task<bool> ConnectAsync(string ssid)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to disconnect from current wifi network
        /// </summary>
        /// <returns>True if disconnected, false if failed.</returns>
        public Task<bool> DisconnectAsync()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to scan for available wifi networks
        /// </summary>
        /// <returns>True if scanning started, False if error</returns>
        public async Task<bool> ScanWifiAsync()
        {
            if (_scanTokenSource != null)
                return true;
            var threadCheck = new TaskCompletionSource<bool>();
            _scanTokenSource = new CancellationTokenSource();
            _scanTokenSource.Token.Register(() => { _scanTokenSource = null; });
            Task.Run(async () =>
            {
                while (_scanTokenSource != null && !_scanTokenSource.IsCancellationRequested)
                {
                    var scanArg = $"{_nativeCommands.WPA_SCAN}";
                    var scanResultsArg = $"{_nativeCommands.WPA_SCAN_RESULTS}";
                    if (!await RunWPAProcessForBoolResult(scanArg))
                    {
                        DebugOutput($"Failed to initialize wifi scan!", level: DEBUG_LEVEL.ERROR);
                        threadCheck.TrySetResult(false);
                        _scanTokenSource.Cancel();
                        return;
                    }
                    threadCheck.TrySetResult(true);
                    await Task.Delay(_nativeConfiguration.WifiScanTime);
                    var response = await RunWPAProcessForMultiLineResult(scanResultsArg, 2);
                    if (response != null && response.Count > 1)
                        ParseAvailableNetworks(response);
                }
            }, _scanTokenSource.Token);
            return await threadCheck.Task;
        }

        /// <summary>
        /// Method to stop scanning for available wifi networks
        /// </summary>
        /// <returns>True if stopped, false if error</returns>
        public Task<bool> StopScanWifiAsync()
        {
            if (_scanTokenSource == null)
                return Task.FromResult(true);
            try
            {
                _scanTokenSource.Cancel();
                _scanTokenSource.Dispose();
                _scanTokenSource = null;
            }catch(Exception ex)
            {
                DebugOutput("Failed to cancel wifi scanning", level: DEBUG_LEVEL.ERROR);
                return Task.FromResult(false);
            }
            return Task.FromResult(true);
        }

        /// <summary>
        /// Method to implement <see cref="IDisposable.Dispose"/> functionality
        /// </summary>
        public async void Dispose()
        {
            if (_scanTokenSource != null)
                await StopScanWifiAsync();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Method to parse the response from wpa_cli containing the available networks
        /// </summary>
        /// <param name="response"><see cref="List{T}"/> of strings returned by wpa_cli application</param>
        private void ParseAvailableNetworks(List<string> response)
        {
            if (AvailableNetworks == null)
                AvailableNetworks = new ObservableCollection<IAvailableNetwork>();
            var foundNetworks = new List<IAvailableNetwork>();
            foreach (string line in response)
            {
                var splitLine = line.Split(_nativeCommands.WPA_OUTPUT_DELIMITER);
                var newNetwork = new AvailableNetwork(splitLine[splitLine.Length], splitLine[0], splitLine[1], splitLine[2], splitLine[3]);
                foundNetworks.Add(newNetwork);
            }
            for(int i = 0; i < foundNetworks.Count; i++)
            {
                if (AvailableNetworks.Any(network => network.SSID.Equals(foundNetworks[i].SSID)))
                    continue;
                var newNetwork = new AvailableNetwork(foundNetworks[i].SSID, foundNetworks[i].MAC, foundNetworks[i].Freq, foundNetworks[i].SignalLevel, foundNetworks[i].Flags);
                AvailableNetworks.Add(newNetwork);
            }
            for(int i = AvailableNetworks.Count; i > 0; i--)
            {
                if (!foundNetworks.Any(network => network.SSID.Equals(network.SSID)))
                    AvailableNetworks.RemoveAt(i);
            }
        }

        /// <summary>
        /// Method to parse information from wpa_cli output to form an <see cref="IAvailableNetwork"/> object representing currently connected wifi network
        /// </summary>
        /// <param name="response"><see cref="List{T}"/> of strings from wpa_cli output</param>
        /// <returns><see cref="IAvailableNetwork"/> representing currently connected network</returns>
        private IAvailableNetwork ParseCurrentNetwork(List<string> response)
        {
            if (response == null || response.Count < 1)
                return null;
            var mac = "";
            var ip = "";
            var ssid = "";
            var security = "";
            var freq = "";
            var macLine = response.FirstOrDefault(str => str.Contains(_nativeCommands.MAC_ID));
            if (!string.IsNullOrEmpty(macLine))
                mac = macLine.Split(_nativeCommands.NETWORK_INFO_DELIMITER)[1];
            var ipLine = response.FirstOrDefault(str => str.Contains(_nativeCommands.IP_ID));
            if (!string.IsNullOrEmpty(ipLine))
                ip = ipLine.Split(_nativeCommands.NETWORK_INFO_DELIMITER)[1];
            var ssidLine = response.FirstOrDefault(str => str.Contains(_nativeCommands.SSID_ID));
            if (!string.IsNullOrEmpty(ssidLine))
                ssid = ssidLine.Split(_nativeCommands.NETWORK_INFO_DELIMITER)[1];
            var secLine = response.FirstOrDefault(str => str.Contains(_nativeCommands.SECURITY_ID));
            if (!string.IsNullOrEmpty(secLine))
                security = secLine.Split(_nativeCommands.NETWORK_INFO_DELIMITER)[1];
            var freqLine = response.FirstOrDefault(str => str.Contains(_nativeCommands.FREQ_ID));
            if (!string.IsNullOrEmpty(freqLine))
                freq = freqLine.Split(_nativeCommands.NETWORK_INFO_DELIMITER)[1];
            return new AvailableNetwork(ssid, mac, freq, "", security);
        }

        /// <summary>
        /// Method to parse the current state from wpa_cli status output. Compares state to passed value.
        /// </summary>
        /// <param name="response">wpa_cli status output response</param>
        /// <param name="compareTo">string value to evaluate state against</param>
        /// <returns>True if state contains passed value, otherwise false</returns>
        private bool CheckWifiState(List<string> response, string compareTo)
        {
            var stateLine = response.FirstOrDefault(str => str.Contains(_nativeCommands.STATE_ID));
            if (string.IsNullOrEmpty(stateLine))
            {
                DebugOutput("Failed to get wifi state key-value pair", level: DEBUG_LEVEL.WARN);
                return false;
            }
            var state = stateLine.Split(_nativeCommands.NETWORK_INFO_DELIMITER);
            if (state.Length < 2)
            {
                DebugOutput("Failed to get wifi state key", level: DEBUG_LEVEL.WARN);
                return false;
            }
            if (!state.Contains(compareTo))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Method to run the wpa_cli and return a multi-line response as <see cref="List{T}"/> of strings. Allows for skipping n lines beginning at index 0.
        /// </summary>
        /// <param name="argument">Argument to pass to wpa_cli</param>
        /// <returns><see cref="List{T}"/> of strings representing lines of output</returns>
        private Task<List<string>> RunWPAProcessForMultiLineResult(string argument, int linesToSkip = 0)
        {
            var response = "";
            var process = CreateWPAProcessWithOutput(argument);
            process.OutputDataReceived += (sender, args) =>
            {
                response = args.Data;
            };
            process.Start();
            process.BeginOutputReadLine();
            process.WaitForExit();
            return Task.FromResult(SplitWPAResponse(response, linesToSkip, Environment.NewLine));
        }
        
        /// <summary>
        /// Method to run the wpa_cli and return a single string result from wpa_cli output
        /// </summary>
        /// <param name="argument">Command line argument to pass to wpa_cli</param>
        /// <returns>Single string result from wpa_cli output</returns>
        private Task<string> RunWPAProcessForStringResult(string argument)
        {
            var response = "";
            var process = CreateWPAProcessWithOutput(argument);
            process.OutputDataReceived += (sender, args) =>
            {
                response = args.Data;
            };
            process.Start();
            process.BeginOutputReadLine();
            process.WaitForExit();
            return Task.FromResult(GetWPAReturnCode(response));
        }

        /// <summary>
        /// Method to run the wpa_cli application and return a boolean value based on wpa_cli output
        /// </summary>
        /// <param name="argument">Command line argument to pass to wpa_cli</param>
        /// <returns>True if wpa_cli returns 'OK' - False if wpa_cli returns 'FAIL'</returns>
        private Task<bool> RunWPAProcessForBoolResult(string argument)
        {
            var response = "";
            var process = CreateWPAProcessWithOutput(argument);
            process.OutputDataReceived += (sender, args) =>
            {
                response = args.Data;
            };
            process.Start();
            process.BeginOutputReadLine();
            process.WaitForExit();
            return Task.FromResult(GetWPAReturnCode(response).Contains(_nativeCommands.WPA_FAILED) ? false : true);
        }

        /// <summary>
        /// Method to create a <see cref="Process"/> targeting the wpa_cli binary specified by <see cref="INativeCommands.WPA_CLI_BINARY"/>
        /// </summary>
        /// <param name="argument">Command line argument to pass to wpa_cli</param>
        /// <returns><see cref="Process"/> instance</returns>
        private Process CreateWPAProcessWithOutput(string argument)
        {
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = _nativeCommands.WPA_CLI_BINARY,
                    Arguments = argument,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardOutput = true
                }
            };
            return process;
        }

        /// <summary>
        /// Method to parse the response from wpa_cli application. Ignores the first line of output which is the selected device interface (eg. wlan0)
        /// </summary>
        /// <param name="response">Full response from wpa_cli</param>
        /// <returns>
        /// Second line of output from wpa_cli - ignores first line which is selected device interface. 
        /// "FAIL" if response is empty or only one line was returned
        /// </returns>
        private string GetWPAReturnCode(string response)
        {
            if (string.IsNullOrEmpty(response))
                return _nativeCommands.WPA_FAILED;
            var parsed = response.Split(Environment.NewLine);
            if (parsed.Length < 2)
                return _nativeCommands.WPA_FAILED;
            else
                return parsed[1];
        }

        /// <summary>
        /// Method to split the response from wpa_cli. Allows for skipping n lines at the beginning of output and specify delimiter.
        /// </summary>
        /// <param name="response">String to split</param>
        /// <param name="linesToSkip">Number of lines to skip - these are sequential lines starting at index 0</param>
        /// <param name="delim">Char to use as delimiter</param>
        /// <returns></returns>
        private List<string> SplitWPAResponse(string response, int linesToSkip = 0, string delim = " ")
        {
            if (string.IsNullOrWhiteSpace(response))
                return null;
            List<string> result = new List<string>();
            var parsed = response.Split(delim);
            for (int i = linesToSkip; i < parsed.Length; i++)
                result.Add(parsed[i]);
            return result;
        }

        /// <summary>
        /// Method to offload updating the list of known networks
        /// </summary>
        private void LazyUpdateKnownNetworks()
        {
            Task.Run(async () =>
            {
                _isUpdatingKnownNetworks = true;
                _knownNetworks = await GetKnownNetworksAsync();
                _isUpdatingKnownNetworks = false;
            });
        }

        /// <summary>
        /// Method to offload a thread for watching the wifi connection state
        /// </summary>
        private void MonitorConnectionState()
        {
            if (_connectedMonitor != null)
            {
                _connectedMonitor.Cancel();
                _connectedMonitor.Dispose();
                _connectedMonitor = null;
            }
            _connectedMonitor = new CancellationTokenSource();
            Task.Run(async () =>
            {
                while (_connectedMonitor != null && !_connectedMonitor.IsCancellationRequested)
                {
                    var stateArg = $"{_nativeCommands.WPA_STATUS}";
                    var response = await RunWPAProcessForMultiLineResult(stateArg, 1);
                    if (response == null || response.Count < 1)
                    {
                        DebugOutput("Wifi State returned empty", level: DEBUG_LEVEL.WARN);
                        CurrentNetwork = null;
                        continue;
                    }
                    if (!CheckWifiState(response, _nativeCommands.WPA_CONNECTED))
                    {
                        CurrentNetwork = null;
                        continue;
                    }
                    CurrentNetwork = ParseCurrentNetwork(response);
                    await Task.Delay(_nativeConfiguration.WifiStateWaitTime);
                }
            }, _connectedMonitor.Token);
        }

        /// <summary>
        /// Method to log errors to the console
        /// </summary>
        /// <param name="message">Message to display</param>
        /// <param name="caller">Member or method that threw the error</param>
        private void DebugOutput(string message, [CallerMemberName]string caller = "", DEBUG_LEVEL level = DEBUG_LEVEL.INFO)
        {
            Console.WriteLine($"{level.ToString()} => {caller}: {message}");
        }

        #endregion
    }
}
