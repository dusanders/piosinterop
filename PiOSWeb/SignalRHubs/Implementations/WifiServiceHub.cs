﻿using Microsoft.AspNetCore.SignalR;
using PiOSInterop.Interfaces.Wifi;
using PiOSWeb.SignalRHubs.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PiOSWeb.SignalRHubs
{
    public class WifiServiceHub : Hub, IWifiServiceHub
    {
        private INativeWifiService _wifiService;

        public WifiServiceHub(INativeWifiService wifiService)
        {
            _wifiService = wifiService;
        }

        #region Public Methods

        public override Task OnConnectedAsync()
        {
            AssignId(Context.ConnectionId);
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            return base.OnDisconnectedAsync(exception);
        }

        #endregion

        #region Client Methods

        [HubMethodName("ClientRequest_IsConnected")]
        public async void ClientRequest_IsConnected(string id)
        {
            await Clients.Client(id).SendCoreAsync("IsConnected", new object[] { _wifiService.IsConnected });
        }

        [HubMethodName("ClientRequest_StartWifiScan")]
        public void ClientRequest_StartWifiScan(string id)
        {
            throw new NotImplementedException();
        }

        [HubMethodName("ClientRequest_StopWifiScan")]
        public void ClientRequest_StopWifiScan(string id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Private Methods

        private async void AssignId(string id)
        {
            await Clients.Client(id).SendCoreAsync("AssignId", new object[] { id });
        }

        #endregion
    }
}
