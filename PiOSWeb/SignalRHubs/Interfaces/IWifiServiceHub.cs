﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PiOSWeb.SignalRHubs.Interfaces
{
    public interface IWifiServiceHub
    {
        Task OnConnectedAsync();
        Task OnDisconnectedAsync(Exception exception);
        void ClientRequest_IsConnected(string id);
        void ClientRequest_StartWifiScan(string id);
        void ClientRequest_StopWifiScan(string id);
    }
}
