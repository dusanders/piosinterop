﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PiOSInterop.Implementations.NativeProcess;
using PiOSInterop.Implementations.Wifi;
using PiOSInterop.Interfaces.NativeProcess;
using PiOSInterop.Interfaces.Wifi;
using PiOSWeb.Configurations;
using PiOSWeb.Configurations.Interfaces;
using PiOSWeb.SignalRHubs;
using PiOSWeb.SignalRHubs.Interfaces;
using System.IO;

namespace PiOSWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var nativeDevice = new NativeConfiguration();
            var wifiClientMethods = new WifiClientConstants();
            var nativeCommands = new NativeCommands();
            Configuration.Bind("NativeConfiguration", nativeDevice);
            Configuration.Bind("WifiPushClientMethods", wifiClientMethods);
            Configuration.Bind("NativeCommands", nativeCommands);
            services.AddSingleton<IWifiClientConstants>(wifiClientMethods);
            services.AddSingleton<INativeConfiguration>(nativeDevice);
            services.AddSingleton<INativeCommands>(nativeCommands);
            services.AddSingleton<INativeWifiService, NativeWifiService>();
            services.AddSingleton<IWifiServiceHub, WifiServiceHub>();
            services.AddCors();
            services.AddSignalR();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.Use(async (context, next) =>
            {
                await next();
                if(context.Response.StatusCode == 404 &&
                    !Path.HasExtension(context.Request.Path.Value) &&
                    !context.Request.Path.Value.StartsWith("/api/"))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
                if (context.Request.Path.Value.StartsWith("/api/"))
                {
                    context.Request.Path = "/index.html";
                }
            });
            app.UseCors(builder =>
            {
                builder.WithOrigins("http://localhost:4200")
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
            });
            app.UseSignalR(routes =>
            {
                routes.MapHub<WifiServiceHub>("/wifiservicehub");
            });
            app.UseMvcWithDefaultRoute();
            app.UseDefaultFiles();
            app.UseStaticFiles();
        }
    }
}
