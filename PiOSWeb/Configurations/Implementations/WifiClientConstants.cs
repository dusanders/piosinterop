﻿using PiOSWeb.Configurations.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PiOSWeb.Configurations
{
    public class WifiClientConstants : IWifiClientConstants
    {
        public string WifiStateChangeMethod { get; set; }
    }
}
