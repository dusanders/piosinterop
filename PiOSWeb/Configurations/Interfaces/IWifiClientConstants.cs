﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PiOSWeb.Configurations.Interfaces
{
    public interface IWifiClientConstants
    {
        string WifiStateChangeMethod { get; }
    }
}
