﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PiOSInterop.Interfaces.Wifi;

namespace PiOSWeb.Controllers
{
    [Produces("application/json")]
    [Route("api/WifiSettings")]
    public class WifiSettingsController : Controller
    {
        private INativeWifiService _wifiService;
        public WifiSettingsController(INativeWifiService wifiService)
        {
            _wifiService = wifiService;
        }

        [HttpGet]
        [Route("isconnected")]
        public Task<bool> IsConnected()
        {
            return Task.FromResult(_wifiService.IsConnected);
        }
    }
}