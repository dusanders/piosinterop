﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PiOSWeb.PushServices.Interfaces
{
    public interface IWifiPushService
    {
        Task WifiStateChanged(bool state);
    }
}
