﻿using Microsoft.AspNetCore.SignalR;
using PiOSInterop.Interfaces.Wifi;
using PiOSWeb.Configurations.Interfaces;
using PiOSWeb.PushServices.Interfaces;
using PiOSWeb.SignalRHubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PiOSWeb.PushServices.Implementations
{
    public class WifiPushService : IWifiPushService, IDisposable
    {
        private IHubContext<WifiServiceHub> _context;
        private INativeWifiService _wifiService;
        private IWifiClientConstants _clientMethods;

        public WifiPushService(IHubContext<WifiServiceHub> wifiServiceContext,
                                INativeWifiService wifiService,
                                IWifiClientConstants clientMethods)
        {
            _context = wifiServiceContext;
            _wifiService = wifiService;
            _clientMethods = clientMethods;
            _wifiService.ConnectedStateChanged += WifiConnectionChangedEventHandler;
        }

        public void Dispose()
        {
            if (_wifiService != null)
                _wifiService.ConnectedStateChanged -= WifiConnectionChangedEventHandler;
        }

        public async Task WifiStateChanged(bool state)
        {
            await _context.Clients.All.SendCoreAsync(_clientMethods.WifiStateChangeMethod, new object[] { state });
        }

        private async void WifiConnectionChangedEventHandler(object sender, bool e)
        {
            await WifiStateChanged(e);
        }
    }
}
