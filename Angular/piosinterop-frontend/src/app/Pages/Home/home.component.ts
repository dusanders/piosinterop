import {Component} from '@angular/core'
import { WifiService } from '../../Services/WifiService';
import { HomeViewModel } from '../../Models/HomeViewModel';

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})

export class HomeComponent{

    private _viewModel: HomeViewModel

    public Title: string = "Home";
    public WifiServiceHubConnected: boolean = false;

    constructor(viewModel: HomeViewModel){
        this._viewModel = viewModel;
        this._viewModel.WifiHubConnectionChanged.subscribe((state: boolean) => {
            this.WifiServiceHubConnectionStateChange(state);
        })
    }

    private WifiServiceHubConnectionStateChange(state: boolean){
        this.WifiServiceHubConnected = state;
    }
}