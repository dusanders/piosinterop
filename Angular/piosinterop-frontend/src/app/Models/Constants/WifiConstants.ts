import {Injectable} from '@angular/core'

@Injectable()

export class WifiConstants{
    public WifiServiceHubRoute: string = "wifiservicehub";
    public IsConnectedMethod: string = "isconnected";
    public StartWifiScanMethod: string = "startwifiscan";
    public StopWifiScanMethod: string = "stopwifiscan";
}