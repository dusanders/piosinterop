import {Injectable} from '@angular/core'

@Injectable()

export class AppConstants{
    public Host: string;
    public HostPort: number;
    public ConnectionID: string;

    constructor(){
        this.HostPort = 5000;
        this.Host = "http://" + location.hostname + ":" + this.HostPort + "/";
    }
}