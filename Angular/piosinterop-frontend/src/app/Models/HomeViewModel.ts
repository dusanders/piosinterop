import {Injectable} from '@angular/core'
import { WifiService } from '../Services/WifiService';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()

export class HomeViewModel{
    private _wifiServiceHub: WifiService;

    public WifiHubConnectionChanged: BehaviorSubject<boolean>;

    constructor(wifiService: WifiService){
        this._wifiServiceHub = wifiService;
        this.WifiHubConnectionChanged = new BehaviorSubject(false);
        this._wifiServiceHub.HubConnected.subscribe((state) => {
            this.WifiHubConnectionStateChanged(state);
        });
        this._wifiServiceHub.Connect();
    }

    private WifiHubConnectionStateChanged(state: boolean){
        this.WifiHubConnectionChanged.next(state);
    }
}