import {Injectable} from '@angular/core'
import {BehaviorSubject} from 'rxjs';
import {HubConnection, HubConnectionBuilder} from '@aspnet/signalr';
import { WifiConstants } from '../Models/Constants/WifiConstants';
import { AppConstants } from '../Models/Constants/AppConstants';

@Injectable()

export class WifiService{
    private _wifiConstants: WifiConstants;
    private _appConstants: AppConstants;
    private _connection: HubConnection

    public HubConnected: BehaviorSubject<boolean>;
    public WifiConnected: BehaviorSubject<boolean>;
    public IsActive: BehaviorSubject<boolean>;
    public IsScanning: BehaviorSubject<boolean>;

    constructor(appConstants: AppConstants,
                wifiConstants: WifiConstants){
        this._appConstants = appConstants;
        this._wifiConstants = wifiConstants;
        this.HubConnected = new BehaviorSubject(false);
        this.WifiConnected = new BehaviorSubject(false);
        this.IsActive = new BehaviorSubject(false);
        this.IsScanning = new BehaviorSubject(false);
    }

    public Connect(): void{
        var connectionString = this._appConstants.Host + this._wifiConstants.WifiServiceHubRoute;
        this._connection = new HubConnectionBuilder().withUrl(connectionString).build();
        this._connection.on("wifistatechanged", (state) => { this.WifiConnectionStateChange(state); });
        this._connection.on("isactivechanged", (state) => { this.IsActiveStateChange(state); });
        this._connection.on("scanningstatechange", (state) => { this.IsScanningStateChange(state); });
        this._connection.onclose((error: Error) => { this.ConnectionClosed(error); });
        this._connection.start()
            .then((value: any) =>{
                this.ConnectionOpen();
            })
            .catch((error: any) =>{
                this.ConnectionClosed(error);
            })
    }

    private IsScanningStateChange(state: boolean): void{
        this.IsScanning.next(state);
    }

    private IsActiveStateChange(state: boolean): void{
        this.IsActive.next(state);
    }

    private WifiConnectionStateChange(state: boolean) : void{
        this.WifiConnected.next(state);
    }

    private ConnectionOpen(): void{
        this.HubConnected.next(true);
    }

    private ConnectionClosed(error: Error): void{
        this.HubConnected.next(false);
        console.log("WifiService error: " + error);
    }
}