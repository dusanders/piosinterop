
// Angular/NPM imports
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {RouterModule} from '@angular/router';

// Pages imports
import { AppComponent } from './app.component';
import {NavMenuComponent} from './Pages/NavMenu/navmenu.component';
import {HomeComponent} from './Pages/Home/home.component';
import {SettingsComponent} from './Pages/Settings/settings.component';
import { AppConstants } from './Models/Constants/AppConstants';
import { WifiConstants } from './Models/Constants/WifiConstants';
import {WifiService} from './Services/WifiService';
import {HomeViewModel} from './Models/HomeViewModel';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'settings',
        component: SettingsComponent
      }
    ])
  ],
  providers: [
    AppConstants,
    WifiConstants,
    WifiService,
    
    HomeViewModel
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
